# HALEO TEST

App reads information of a local JSON file called `data.json` located within the `src` folder

Making changes to the JSON file and saving will make changes to the displayed page in real time.

Only data from the `answers` section of the JSON is used in the app. If an answer for the same questionId is given in both the Therapy questionnaire and Screener only the Therapy questionnaire answer will be used.

This app was created under the assumption that the Therapy questionnaire will always appear second in the JSON. If this is not the case there will be bugs. The current implimentation was chosen due to it having a faster runtime.

### `npm install`

Installs dependencies

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
