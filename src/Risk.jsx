import React, { Component } from "react";

class Risk extends Component {
  constructor(props) {
    super(props)
  }


  render() {

    //name defaults to error if questionId != 1,2,3,4
    var name = "Error"

    //risk determines the CSS styling to be used

    //Positive = filled in blue
    //Negative = white background, light blue border
    var risk = "Positive Risk"

    if (this.props.risk.value === "never" || this.props.risk.value === "lowRisk"){
      risk = "Negative Risk"
    }

    //chooses display name based on questionId
    if (this.props.risk.questionId === 1){
      name = "RLS Risk"
    } else if (this.props.risk.questionId === 2){
      name = "SWSD Risk"
    } else if (this.props.risk.questionId === 3){
      name = "Caffeine"
    } else if (this.props.risk.questionId === 4){
      name = "Alcohol"
    }

    return(
      <div className={risk} >
        <p>{name}</p>
      </div>
    )
  }

}

export default Risk;