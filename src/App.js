import React, { Component } from 'react';
import './App.css';
import Risk from './Risk.jsx'

class App extends Component {
  constructor() {
    super()
    this.state = {
      userRisks: []
    }

  }
  componentDidMount() {
    this.getJson();
  }


  getJson() {
    //placeholder for reading json file locally
    const data = require('./data.json');
    const usedIds =[]
    const risks = []
    //pushes all of the newest answers
    data[1].answers.forEach(answer => {
      usedIds.push(answer.questionId)
      risks.push(answer)
    })
    //backfills any missing answers, skipping newer answers
    data[0].answers.forEach(answer => {
      if (!usedIds.includes(answer.questionId)){
        risks.push(answer)
      }
    })
    //sorts answers by questionId to ensure risks always displayed in same order
    risks.sort(function(a, b) {
      return a.questionId - b.questionId
    })
    this.setState({userRisks: risks})
  }

  render() {
    return (
      <div className="App">
        <h1>Alerts</h1>
        <div className="RiskContainer">
          {this.state.userRisks.map((risk, index) => {
            return(
              <React.Fragment key={index}>
                <Risk
                  risk={risk}
                />
              </React.Fragment>
              )

            })
          }
        </div>
      </div>
    );
  }
}

export default App;
